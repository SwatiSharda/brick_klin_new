<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The database table used by the model.
     * @var string
     */
     protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'email',
        'gst_no',
        'state',
        'state_code',
        'vehicle_no',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
        'deleted_at'
    ];


    /**
     * Scope a query to only include active users.
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate bank
     *
     * @param array $inputs
     * @param int $id
     *
     * @return \Illuminate\Validation\Validator
     **/
    public function validateCustomer($inputs, $id = null)
    {
        // validation rule
        if ($id) {
            $rules['name'] = 'required|unique:name,address,email,gst_no,' . $id . ',id,deleted_at,NULL';
        }
        else {
            $rules['name'] = 'required|unique:name,address,email,gst_no,NULL,id,deleted_at,NULL';
        }

        //$rules['account_holder'] = 'required';

        return validate($inputs,$rules);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param array $inputs
     * @param int $id
     *
     * @return mixed
     */
     public function store($inputs, $id = null)
    {
        if ($id) {
            return $this->find($id)->update($inputs);
        } else {
            return $this->create($inputs)->id;
        }
    }
    /**
     * Method is used to search news detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getCustomer($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : '20';
        // default filter if no search
        $filter = 1;

        if (is_array($search) && count($search) > 0) {
            $customer = (array_key_exists('keyword', $search)) ? " AND name LIKE '%" .
                addslashes($search['keyword']) . "%' " : "";
            $filter .= $customer;
        }

        return $this->whereRaw($filter)->get();
    }

    /**
     * Method is used to get total bank search wise.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalCustomer($search = null)
    {
        // if no search add where
        $filter = 1;

        // when search news
        if (is_array($search) && count($search) > 0) {
            $customer = (array_key_exists('name', $search)) ? " AND name LIKE '%" .
                addslashes($search['name']) . "%' " : "";
            $filter .= $customer;
        }

        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }

    /**
     * @param int $id
     * @return int
     */
     public function drop($id)
    {
        $this->find($id)->update([ 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => 1]);
    }

    /**
     * @return mixed
     */
    public function getCustomerService()
    {
        $data = $this->active()->get(['id','name', 'address','vehicle_no']);
        $result = [];
        foreach($data as $detail) {
            $result[$detail->id]  = $detail->name . ' - ' . $detail->address . ' - ' . $detail->vehicle_no;
        }
        return $result;
    }

}
