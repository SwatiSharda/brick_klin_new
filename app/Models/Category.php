<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'category_name',

        'status',
        'created_by',
        'updated_by',
        'deleted_by',
        'deleted_at'
    ];



    /**
     * Scope a query to only include active users.
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate
     * @param int $id
     * @return Response
     **/
    public function validateCategory($inputs, $id = null)
    {
        if ($id) {
            $rules['category_name'] = 'required|unique:categories,category_name,' . $id .',id,deleted_at,NULL';
        } else {
            $rules['category_name'] = 'required|unique:categories,category_name,NULL,id,deleted_at,NULL';
        }
        return validate($inputs, $rules);
    }

    /**
     * Method is used to save/update resource.
     * @param   array $input
     * @param   int $id
     * @return  Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     * @return mixed
     */
    public function getCategory($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : '20';
        // default filter if no search
        $filter = 1;

        // $fields = [
        //     'id',
        //     'name',
        //     'code',
        //     'description',
        //     'status',
        // ];

        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search))?
                " AND (name LIKE '%".addslashes(trim($search['keyword'])).
                "%' OR code LIKE '%".addslashes(trim($search['keyword']))."%')"
                : "";
            $filter .= $f1;
        }
        return $this->whereRaw($filter)->get();
    }

    /**
     * Method is used to get total results.
     * @param array $search
     *
     * @return mixed
     */
    public function totalCategory($search = null)
    {
        $filter = 1; // if no search add where
        // when search
        if (is_array($search) && count($search) > 0) {
            $name = (array_key_exists('keyword', $search)) ? " AND name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $name;
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)
            ->first();
    }

    /**
     * @return mixed
     */
    public function getCategoryService()
    {
        $data = $this->active()
            ->orderBy('name','ASC')
            ->get(['name', 'id']);
        $result = [];
        foreach($data as $detail) {
            $result[$detail->id] = $detail->name;

        }
        return ['' =>'-Select Category-'] + $result;
    }

    /**
     * @param $id
     */
    public function drop($id)
    {
        $this->find($id)->update( [ 'deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => authUserId()]);
    }

}
