<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cat = (new Category)->getCategory($search = null, $skip = null, $perPage = null);
         if($cat) {
            return response()->json([
                "message" => "Data Found Successfully",
                "code"    => 200,
                "data"    =>$cat,
            ]);
        } else  {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cat = new category;
        $cat->category_name = $request->cat_name;
        

        $result = $cat->save();
        if($result) {
            return response()->json([
                'message' => "Data Inserted Successfully",
                "code"    => 200
            ]);
        } else  {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        return view('product_category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Request $request)
    {
        //
         //$result = (new Category)->find($id);
         $result = Category::where('id',$request->id)->first();
        if(!$result) {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500 
            ]);
        } 
        return response()->json([
            'message' => "Data Found",
            "code"    => 200,
            "data"    =>$result
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $result = Category::where('id',$request->id)->update([
            'category_name'=>$request->edit_cat,
            ]);

        if($result){
            return response()->json([
                'message' => "Data Updated Successfully",
                'code' => 200,
            ]);

        }else{
            return response()->json([
                'message' => "Internal Server Error",
                'code'=>500
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function drop(Request $request,$id)
    {
        $id = $request->id;
        $cat = (new Category)->find($id);
        if (!$cat) {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }

        (new Category)->drop($id);
        //$result = bank_detail::where('id', $request->id)->delete();

        return response()->json([
            'message' => "Data Deleted Successfully!",
            "code"    => 200,
        ]);
    }
}
