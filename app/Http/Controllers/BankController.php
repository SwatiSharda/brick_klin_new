<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\BankDetail;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = (new BankDetail)->getBanks($search = null, $skip = null, $perPage = null);
        return view('banks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $inputs = $request->all();
        $validator = (new BankDetail)->validateBank($inputs);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->messages(),
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }

        try 
        {
            $inputs = $inputs + [
                'status' => isset($inputs['status']) ? 1 : 0,
                'created_by' => \Auth::user()->id
            ];
            (new BankDetail)->store($inputs);

            return response()->json([
                'message' => "Data Inserted Successfully",
                "code"    => 200
            ]);

        } catch (\Exception $exception) 
        {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        // $bank = (new Bank)->find($id);
        $bank = BankDetail::where('id', $request->id)->first();
        if (!$bank) {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }
            
        return response()->json([
            'message' => "Data Found",
            "code"    => 200,
            "data"    =>$bank  
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $result =BankDetail::where('id',$request->id)->update([
            'bank_name'=>$request->edit_name,
            'account_no'=>$request->edit_acc,
            'ifsc_code' =>$request->edit_ifsc,
            'micr_code' =>$request->edit_micr
        ]);

        if($result){
            return response()->json([
                'message' => "Data Updated Successfully",
                'code' => 200,
            ]);

        }else{
            return response()->json([
                'message' => "Internal Server Error",
                'code'=>500
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function drop(Request $request,$id)
    {
        $id = $request->id;
        $bank = (new BankDetail)->find($id);
        if (!$bank) {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }

        (new BankDetail)->drop($id);
        //$result = bank_detail::where('id', $request->id)->delete();

        return response()->json([
            'message' => "Data Deleted Successfully!",
            "code"    => 200,
        ]);
    }
}
