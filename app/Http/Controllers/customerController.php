<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Customer;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cus = (new Customer)->getCustomer($search = null, $skip = null, $perPage = null);

        if($cus) {
            return response()->json([
                "message" => "Data Found Successfully",
                "code"    => 200,
                "data"    =>$cus
            ]);
        } else  {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $validator = (new Customer)->validate($inputs);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->messages(),
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }

        try 
        {
            $inputs = $inputs + [
                'status' => isset($inputs['status']) ? 1 : 0,
                'created_by' => \Auth::user()->id
            ];
            (new Customer)->store($inputs);

            return response()->json([
                'message' => "Data Inserted Successfully",
                "code"    => 200
            ]);

        } catch (\Exception $exception) 
        {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        return view('customer_details');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $cus = Customer::where('id',$request->id)->first();
        if (!$cus) {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }
            
        return response()->json([
            'message' => "Data Found",
            "code"    => 200,
            "data"    =>$cus  
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $result =Customer::where('id',$request->id)->update([
            'name'=>$request->edit_name,
            'address'=>$request->edit_address,
            'email' =>$request->edit_email,
            'gst_no'   =>$request->edit_gstno,
            'state'   =>$request->edit_state, 
            'state_code'=>$request->edit_statecode,
            'vehicle_no'=>$request->edit_vehicle_no,
            ]);

        if($result){
            return response()->json([
                'message' => "Data Updated Successfully",
                'code' => 200,
            ]);

        }else{
            return response()->json([
                'message' => "Internal Server Error",
                'code'=>500
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function drop(Request $request,$id)
    {
        $id = $request->id;
        $cus = (new Customer)->find($id);
        if (!$cus) {
            return response()->json([
                'message' => "Internal Server Error",
                "code"    => 500
            ]);
        }

        (new Customer)->drop($id);
        //$result = bank_detail::where('id', $request->id)->delete();

        return response()->json([
            'message' => "Data Deleted Successfully!",
            "code"    => 200,
        ]);
    }
    
}
