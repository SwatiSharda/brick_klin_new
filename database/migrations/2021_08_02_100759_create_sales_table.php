<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->string('vehicle_no');
            $table->string('invoice_number');
            $table->date('invoice_date');
            $table->double('amount_before_tax');
            $table->double('cgst_amount');
            $table->double('sgst_amount');
            $table->double('igst_amount');
            $table->double('amount_after_tax');
            $table->double('cgcr');
            $table->double('freight');
            $table->double('labour_tax');
            $table->double('total_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
