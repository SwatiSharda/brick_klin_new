<table class="table table-bordered">
					<thead>
						<tr>
							<th>id</th>
							<th>Bank Name</th>
							<th>IFSC Code</th>
							<th>MICR Code</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>

					@if(is_object($banks) && count($banks) > 0)
						@foreach($banks as $detail)
							<tr>
								<td>{!! $detail->bank_name !!}</td>
								<td>{!! $detail->account_no !!}</td>
								<td>{!! $detail->ifsc_code !!}</td>
								<td>{!! $detail->micr_code !!}</td>
								<td>
									<a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit" href="{!! route('bank.edit', $detail->id) !!}"><i class="fa fa-pencil-alt"></i></a>
									<a class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete" href="{!! route('bank.drop', $detail->id) !!}"><i class="fa fa-pencil-alt"></i></a>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="3" class="text-center">No record found.</td>
						</tr>
					@endif
				</table>